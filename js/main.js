$(document).ready(function(){
    $('.present-slider').slick({
        arrows:false,
        fade:true,
        autoplay: true,
        autoplaySpeed: 3000,
        swipe:false,
        pauseOnHover:false
    });
    
   // $('#paint0_linear').attr('x2',0)
    
    $('.popup-youtube').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });
    
    $('.gallery-link').on('click', function () {
        $(this).next().magnificPopup('open');
    });

    // Initialize Magnific Popup Gallery + Options
    $('.popup-gallery').each(function () {
        $(this).magnificPopup({
            delegate: 'a',
            gallery: {
                enabled: true
            },
            type: 'image'
        });
    });
    $('.image-popup-trigger').magnificPopup({
      type: 'image',
      gallery:{
        enabled:true
      }
    });
    $('.collection').slick({
        arrows:false,
        dots:true
    });
    $('.toggle-trigger').click(function(e){
        e.preventDefault();
        $(this).toggleClass('active');
        $(this).closest('.toggle-widget').find('.toggle-widget__content').stop().slideToggle();
    });
    $('.slide-body-trigger').click(function(e){
        e.preventDefault();
        var el = $(this);
    var dest = el.attr('href'); // получаем направление
    if(dest !== undefined && dest !== '') { // проверяем существование
        $('html').animate({ 
            scrollTop: $(dest).offset().top // прокручиваем страницу к требуемому элементу
        }, 500 // скорость прокрутки
        );
    }
    });
    $('.calc-sidebar-mobile__btn').click(function(){
        $('.detail-order').toggleClass('active');
        $('.overlayer').toggleClass('active')
    });
    $('.overlayer').click(function(e){
        e.preventDefault();
        $('.detail-order').removeClass('active');
        $('.overlayer').removeClass('active')
    })
  
    $('.calc-item__content').matchHeight({byRow:false})
    $('.baner-created-item').click(function(){
        if($(window).width()>990){
        $('.baner-created-item').removeClass('active');
        $(this).addClass('active');
        $('.baner-created__img').removeClass('active');
        $('.baner-created__img').eq($(this).index()+1).addClass('active');
        $('.baner-created-item__desc').stop().slideUp();
        $(this).find('.baner-created-item__desc').stop().slideDown();
        }
    });
    
    $('.inline-popup-trigger').magnificPopup({
          type:'inline',
          midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
        });
    $('.set-item__input,.set-mobile__item input').change(function(){
        
        if($(this).is(':checked')) { 
           
             $.magnificPopup.open({
                items: {
                    src: $('#select-box')
                },
                type: 'inline'
            });
        }
    })
    var flag = 0;
    $('.order-mobile').click(function(){
        if(flag ==0){
            $('body').css('overflow', 'hidden');
            $('body').css('position', 'relative');
            $('body').css('height', '100%');
            $(document).bind('touchmove', false);
            flag = 1;
        }else{
            $('body').css('overflow', 'auto');
            $('body').css('position', 'static');
            $('body').css('height', 'auto');
            $(document).unbind("touchmove");
            flag = 0;
        }
        
        $(this).toggleClass('active');
        $('.order-summ__content').stop().slideToggle();
    })
    if($('.dropzone-item').length){
        var myDropzone = new Dropzone(".dropzone-item", { url: "#",maxFiles: 1});
    }
    
    $('.selective__header').click(function(e){
        e.preventDefault();
        $(".selective").removeClass('active');
        $(this).closest('.selective').toggleClass('active');
    })
    
    $('.match-height').matchHeight();
    $('.custom-scroll').mCustomScrollbar({
        axis:"x"
    });
    $('.mobile-btn-js').click(function(){
        $(this).toggleClass('active');
        $('.mobile-header__content').stop().slideToggle();
    });
    $('.select-widget__header').click(function(e){
        e.preventDefault();
        $(this).closest('.select-widget').find('.select-widget__content').stop().slideToggle();
    });
    $('.select-widget__content .select-widget__item').click(function(e){
        e.preventDefault();
        var markup = $(this).clone();
        $(this).closest('.select-widget').find('.select-widget__content').find('.select-widget__item').removeClass('active');
        $(this).addClass('active');
        $(this).closest('.select-widget').find('.select-widget__header').html(markup);
        $(this).closest('.select-widget').find('.select-widget__content').slideUp();
    });
    
    $('.branding-nav__link').hover(function(){
        var index = parseInt($(this).index())+1;
        $('.branding-nav__link').removeClass('active');
        $(this).addClass('active');
        console.log(index)
        $('.branding-prod__img').removeClass('disable');
        $('.branding-prod__img').not('.branding-prod__img[data-item="'+index+'"]').addClass('disable');
    })
    $('.branding-nav').mouseleave(function(){
        $('.branding-prod__img').removeClass('disable');
    });
    
    $('.set-mobile__trigger').click(function(e){
        e.preventDefault();
        $(this).toggleClass('active');
        $('.set-mobile__content').stop().slideToggle();
    })
    $('.selective-item').click(function(e){
        e.preventDefault();
        
        var $this = $(this).closest('.selective');
        $($this).find('.selective-item').removeClass('selected');
        $(this).addClass('selected');
        
        var title = $(this).find('.puck-select__title').html();
        $($this).find('.selective__title').html(title);
        $('.selective').removeClass('active');
        
    })
    $(document).mouseup(function (e){ // событие клика по веб-документу
		var div = $(".selective"); // тут указываем ID элемента
		if (!div.is(e.target) // если клик был не по нашему блоку
		    && div.has(e.target).length === 0) { // и не по его дочерним элементам
			$(".selective").removeClass('active'); // скрываем его
		}
	});
    
    
    jQuery.fn.ForceNumericOnly =
function()
{
    return this.each(function()
    {
        $(this).keydown(function(e)
        {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 || 
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};
 $(".only-numb").ForceNumericOnly();   
});
$(window).load(function(){
    
  
    $('.masonry-grid').masonry({
  
      itemSelector: '.grid-item',
      // use element for option
      columnWidth: '.grid-item',
      percentPosition: true
    })
})